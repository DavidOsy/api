export interface IRequest {
  id?: number;
  description: string;
  price: number;
  quantity: number;

  createdDate?: Date;
  updatedDate?: Date;
}