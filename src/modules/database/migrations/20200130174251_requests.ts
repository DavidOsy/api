import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable('Requests', table => {
    table.increments('id').primary();
    table.string('description', 250).notNullable();
    table.decimal('price').unsigned().notNullable();
    table.integer('quantity').unsigned().nullable();
    table.dateTime('createdDate').notNullable();
    table.dateTime('updatedDate').notNullable();
  });
}


export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTableIfExists('Requests');
}

