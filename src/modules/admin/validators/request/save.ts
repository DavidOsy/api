import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsNumber,
  MaxLength,
  Min,
  MinLength
} from 'class-validator';
import { IRequest } from 'modules/database/interfaces/Request';

export class SaveValidator implements IRequest {
  @IsOptional()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: false, type: 'integer' })
  public id?: number;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(250)
  @ApiProperty({ required: true, type: 'string', minLength: 5, maxLength: 250 })
  public description: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @ApiProperty({ required: true, type: 'decimal' })
  public price: number;

  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: true, type: 'integer' })
  public quantity: number;
}
